package kz.aitu.oop.examples.practice6Quiz;

public class Singleton {

    private static String str;

    private static Singleton singletonInstance = null;

    public static Singleton getSingleInstance() {
        if(singletonInstance == null) singletonInstance = new Singleton();
        return singletonInstance;
    }


    public void setStr(String str) {
        Singleton.str = str;
    }

    public String getStr() {
        return str;
    }

    @Override
    public String toString() {
        return str;
    }

    private Singleton() {
        str = "hello world";
    }
}
