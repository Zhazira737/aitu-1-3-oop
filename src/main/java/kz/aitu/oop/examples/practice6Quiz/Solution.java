package kz.aitu.oop.examples.practice6Quiz;

public class Solution {
    public static void main(String[] args) {
        Singleton singleton = Singleton.getSingleInstance();
        //singleton.setStr("Hello I am a singleton! Let me say hello world to you");

        //Singleton singleton1 = Singleton.getSingleInstance();
        //singleton.setStr("hello world");
        //System.out.println(singleton);

        System.out.println("Hello I am a singleton! Let me say " + singleton + " to you");
    }
}
