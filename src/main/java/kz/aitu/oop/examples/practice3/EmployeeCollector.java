package kz.aitu.oop.examples.practice3;

public class EmployeeCollector implements Factory{
    @Override
    public Collector createCollector() {
        return new Employee();
    }
}
