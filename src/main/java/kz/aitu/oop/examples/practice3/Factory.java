package kz.aitu.oop.examples.practice3;

public interface Factory {
    Collector createCollector();
}
