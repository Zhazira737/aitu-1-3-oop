package kz.aitu.oop.examples.practice3;

public interface Collector {

    abstract public void updateNameById(String up, int id);

    abstract public void getByCost(int cost);

    abstract public void showTable();

    abstract public String toString();

}
