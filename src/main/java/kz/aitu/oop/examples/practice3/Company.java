package kz.aitu.oop.examples.practice3;

//Factory Design Pattern
public class Company{

    @Override
    public String toString() {
        return "Company{}";
    }

    public static void main(String[] args) {

        Factory factory = byTableName("employee");
        Collector collector =factory.createCollector();
        collector.getByCost(15000);

    }

    static Factory byTableName(String tableName) {
        if(tableName.equalsIgnoreCase("employee")) { //equalIsIgnoreCase --> employee = Employee
            return new EmployeeCollector();
        } else if(tableName.equalsIgnoreCase("project")) {
            return new ProjectCollector();
        } else {
            throw new RuntimeException(tableName + " does not exist!");
        }
    }
}
