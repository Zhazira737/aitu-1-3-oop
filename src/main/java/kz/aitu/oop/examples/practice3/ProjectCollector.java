package kz.aitu.oop.examples.practice3;

public class ProjectCollector implements Factory{
    @Override
    public Collector createCollector() {
        return new Project();
    }
}
