package kz.aitu.oop.examples.practice3;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class Employee implements Collector {
    String url = "jdbc:mysql://localhost:3306/aitu";
    String username = "User2";
    String password = "zo999lo24to";

    private int id;
    private String name;
    private String surname;
    private int age;
    private int projectId;

    private String projectName = null;
    private double projectCost = 0.0;

    @Override
    public void getByCost(int cost) {
        try {
            Connection connection = DriverManager.getConnection(url, username, password);
            Statement statement = connection.createStatement();
           /* ResultSet resultSet = statement.executeQuery("SELECT employee.id, employee.name, surname, age, projectId, project.name, project.cost FROM employee" +
                    " INNER JOIN project " +
                    "ON employee.projectId = project.id WHERE cost = " + cost);*/
            ResultSet resultSet = statement.executeQuery("SELECT employee.id, employee.name, surname, age, " +
                    "projectId, project.name, project.cost FROM employee INNER JOIN project ON " +
                    "employee.projectId = project.id WHERE cost = " + cost);

            while(resultSet.next()) {
                Employee employee = new Employee();
                employee.setId(resultSet.getInt("id"));
                employee.setName(resultSet.getString("name"));
                employee.setSurname(resultSet.getString("surname"));
                employee.setAge(resultSet.getInt("age"));
                employee.setProjectId(resultSet.getInt("projectId"));
                employee.setProjectName(resultSet.getString("project.name"));
                employee.setProjectCost(resultSet.getDouble("project.cost"));

                System.out.println(employee);
            }
            resultSet.close();
            statement.close();
            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateNameById(String name, int id) {
        try {
            Connection connection = DriverManager.getConnection(url, username, password);
            Statement statement = connection.createStatement();
            int resultSet1 = statement.executeUpdate("UPDATE employee SET name=" + name +" WHERE id = " + id);

            ResultSet resultSet = statement.executeQuery("SELECT * FROM employee WHERE id = " + id);
            while(resultSet.next()) {
                Employee employee = new Employee();
                employee.setId(resultSet.getInt("id"));
                employee.setName(resultSet.getString("name"));
                employee.setSurname(resultSet.getString("surname"));
                employee.setAge(resultSet.getInt("age"));
                employee.setProjectId(resultSet.getInt("projectId"));

                System.out.println(employee);
            }

            resultSet.close();
            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void showTable() {
        try {
            Connection connection = DriverManager.getConnection(url, username, password);

            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM employee");

            while(resultSet.next()) {
                Employee employee = new Employee();
                employee.setId(resultSet.getInt("id"));
                employee.setName(resultSet.getString("name"));
                employee.setSurname(resultSet.getString("surname"));
                employee.setAge(resultSet.getInt("age"));
                employee.setProjectId(resultSet.getInt("projectId"));

                System.out.println(employee);
            }
            resultSet.close();
            statement.close();
            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", age=" + age +
                ", projectId=" + projectId +
                ", projectName='" + projectName + '\'' +
                ", projectCost=" + projectCost +
                '}';
    }
}
