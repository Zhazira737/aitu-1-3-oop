package kz.aitu.oop.examples.practice3;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import lombok.Data;

@Data
public class Project implements Collector {
    String url = "jdbc:mysql://localhost:3306/aitu";
    String username = "User2";
    String password = "zo999lo24to";

    private int id;
    private String name;
    private double cost;


    @Override
    public void showTable() {
        try {
            Connection connection = DriverManager.getConnection(url, username, password);

            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM project");

            while(resultSet.next()) {
                //System.out.println(resultSet.getInt("id"));
                Project project = new Project();
                project.setId(resultSet.getInt("id"));
                project.setName(resultSet.getString("name"));
                project.setCost(resultSet.getInt("cost"));

                System.out.println(project);
            }
            resultSet.close();
            statement.close();
            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void getByCost(int cost) {
        this.cost = cost;
        try {
            Connection connection = DriverManager.getConnection(url, username, password);
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM project WHERE cost = " + this.cost);

            while(resultSet.next()) {
                Project project = new Project();
                project.setId(resultSet.getInt("id"));
                project.setName(resultSet.getString("name"));
                project.setCost(resultSet.getInt("cost"));

                System.out.println(project);
            }
            resultSet.close();
            statement.close();
            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateNameById(String up, int id) {
        this.name = up;
        this.id = id;
        try {
            Connection connection = DriverManager.getConnection(url, username, password);
            Statement statement = connection.createStatement();
            int resultSet1 = statement.executeUpdate("UPDATE project SET name=" + this.name +" WHERE id = " + this.id);

            ResultSet resultSet = statement.executeQuery("SELECT * FROM project WHERE id = " + id);
            List<Project> projectList = new ArrayList<>();
            while(resultSet.next()) {
                Project project = new Project();
                project.setId(resultSet.getInt("id"));
                project.setName(resultSet.getString("name"));
                project.setCost(resultSet.getInt("cost"));

                System.out.println(project);
            }

            resultSet.close();
            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        return "Project{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", cost=" + cost +
                '}';
    }
}
