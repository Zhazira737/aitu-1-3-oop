package kz.aitu.oop.examples.practice4.wholeAquarium;

import kz.aitu.oop.examples.practice4.Accessories;
import kz.aitu.oop.examples.practice4.AquariumFactory;
import kz.aitu.oop.examples.practice4.Fish;

public class WholeAquariumFactory implements AquariumFactory {
    @Override
    public Accessories getAccessories() {
        return new AccessoriesForHome();
    }

    @Override
    public Fish getFish() {
        return new FishForHome();
    }
}
