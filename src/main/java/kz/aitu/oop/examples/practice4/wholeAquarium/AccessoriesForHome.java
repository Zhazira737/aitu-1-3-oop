package kz.aitu.oop.examples.practice4.wholeAquarium;

import java.sql.*;
import lombok.Data;

@Data
public class AccessoriesForHome implements kz.aitu.oop.examples.practice4.Accessories {
    String url = "jdbc:mysql://localhost:3306/aitu";
    String username = "User2";
    String password = "aaa";

    private int id;
    private String name;
    private int amount;
    private double cost;
    private int fish_id;
    private double total = 0;


    @Override
    public void showTable() {
        try {
            Connection connection = DriverManager.getConnection(url, username, password);

            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM accessories");

            while(resultSet.next()) {
                AccessoriesForHome accessories = new AccessoriesForHome();
                accessories.setId(resultSet.getInt("id"));
                accessories.setName(resultSet.getString("name"));
                accessories.setAmount(resultSet.getInt("amount"));
                accessories.setCost(resultSet.getDouble("cost"));
                accessories.setFish_id(resultSet.getInt("fish_id"));

                System.out.println(accessories);
            }
            resultSet.close();
            statement.close();
            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void totalCost() {
        try {
            Connection connection = DriverManager.getConnection(url, username, password);

            Statement statement = connection.createStatement();
            FishForHome fish = new FishForHome();
            ResultSet resultSet = statement.executeQuery("SELECT accessories.id, accessories.name, accessories.amount, " +
                    "accessories.cost, accessories.fish_id, (fish.amount * fish.cost + accessories.amount * accessories.cost)"  +
                    "AS total FROM accessories INNER JOIN fish ON accessories.fish_id = fish.id");
            double sum = 0;
            while (resultSet.next()) {
                AccessoriesForHome accessories = new AccessoriesForHome();
                accessories.setId(resultSet.getInt("id"));
                accessories.setName(resultSet.getString("name"));
                accessories.setAmount(resultSet.getInt("amount"));
                accessories.setCost(resultSet.getDouble("cost"));
                accessories.setFish_id(resultSet.getInt("fish_id"));
                accessories.setTotal(resultSet.getDouble("total"));


                System.out.println(accessories);
            }
            resultSet.close();
            statement.close();
            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    @Override
    public String toString() {
        FishForHome fish = new FishForHome();
        return "Accessories{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", amount=" + amount +
                ", cost=" + this.cost +
                ", fish_id=" + fish_id +
                ", total=" + total +
                '}';
    }
}
