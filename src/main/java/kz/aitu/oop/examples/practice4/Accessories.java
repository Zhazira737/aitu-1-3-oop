package kz.aitu.oop.examples.practice4;

public interface Accessories {

    abstract public void showTable();

    abstract public void totalCost();

    abstract public String toString();
}
