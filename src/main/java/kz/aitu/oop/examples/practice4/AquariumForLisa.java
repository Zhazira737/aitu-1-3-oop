package kz.aitu.oop.examples.practice4;

import kz.aitu.oop.examples.practice4.wholeAquarium.WholeAquariumFactory;

public class AquariumForLisa {
    public static void main(String[] args) {

        AquariumFactory aquariumFactory = new WholeAquariumFactory();
        Fish fish = aquariumFactory.getFish();
        Accessories accessories = aquariumFactory.getAccessories();

        fish.totalCost();
        System.out.println("-----------------------------------------");
        fish.showTable();
        System.out.println("-----------------------------------------");


        accessories.showTable();
        System.out.println("-----------------------------------------");
        accessories.totalCost();
        System.out.println("-----------------------------------------");
    }
}
