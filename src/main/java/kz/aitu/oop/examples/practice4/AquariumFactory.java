package kz.aitu.oop.examples.practice4;

public interface AquariumFactory {

    Accessories getAccessories();

    Fish getFish();
}
