package kz.aitu.oop.examples.TicTacToe;

import lombok.Data;

import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Data
public class constructGame implements allMethods {
    static ArrayList<Integer> playerPosition = new ArrayList<Integer>();
    static ArrayList<Integer> computerPosition = new ArrayList<Integer>();

    private int count = 0;
    public String hasWinner(String playerName) {
        List topRow = Arrays.asList(1,2,3);
        List midRow = Arrays.asList(4,5,6);
        List botRow = Arrays.asList(7,8,9);
        List leftCol = Arrays.asList(1,4,7);
        List midCol = Arrays.asList(2,5,8);
        List rightCol = Arrays.asList(3,6,9);
        List cross1 = Arrays.asList(1,5,9);
        List cross2 = Arrays.asList(7,5,3);

        List<List> winning = new ArrayList<List>();

        winning.add(topRow);
        winning.add(midRow);
        winning.add(botRow);
        winning.add(leftCol);
        winning.add(rightCol);
        winning.add(midCol);
        winning.add(cross1);
        winning.add(cross2);

        for(List list : winning) {
            if(playerPosition.containsAll(list)) {
                plusOne(playerName);
                return "YOU WON:)";
            } else if (computerPosition.containsAll(list)) {
                return "YOU LOSE:(";
            } else if (playerPosition.size() + computerPosition.size() == 9) {
                return "DEAD HEAT";
            }
        }
        return "";
    }




    public  void board(char[][] gameBoard) {
        for (char[] row : gameBoard) {
            for (char c : row) {
                System.out.print(c);
            }
            System.out.println();
        }
    }




    public void pane(char[][] gameBoard, int position, String user) {
        char symbol = ' ';
        if(user.equals("player")) {
            symbol = 'X';
            playerPosition.add(position);
        } else if(user.equals("computer")) {
            symbol = 'O';
            computerPosition.add(position);
        }

        switch (position) {
            case 1:
                gameBoard[0][0] = symbol;
                break;
            case 2:
                gameBoard[0][2] = symbol;
                break;
            case 3:
                gameBoard[0][4] = symbol;
                break;
            case 4:
                gameBoard[2][0] = symbol;
                break;
            case 5:
                gameBoard[2][2] = symbol;
                break;
            case 6:
                gameBoard[2][4] = symbol;
                break;
            case 7:
                gameBoard[4][0] = symbol;
                break;
            case 8:
                gameBoard[4][2] = symbol;
                break;
            case 9:
                gameBoard[4][4] = symbol;
                break;
            default:
                break;
        }
    }

    String url = "jdbc:mysql://localhost:3306/aitu";
    String username = "User1";
    String password = "di421am88ond";

    private void plusOne(String playerName) {
        try {
            Connection connection = DriverManager.getConnection(url, username, password);

            Statement statement = connection.createStatement();
            if(statement.execute("SELECT name FROM tictactoe WHERE name = '" + playerName + "'")) {
                statement.executeUpdate("UPDATE tictactoe SET count = count+1 WHERE name = '" + playerName +"'");
            } if (statement.execute("SELECT name FROM tictactoe WHERE name != '" + playerName + "'")) {
                String sql = "INSERT INTO tictactoe (name, count) VALUES ('" + playerName + "', 1)";
                statement.executeUpdate(sql);
            }

            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }



    private String igrok;
    private int counter;

    public void showFive() {

        try {
            Connection connection = DriverManager.getConnection(url, username, password);

            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT name, count FROM tictactoe ORDER BY count DESC LIMIT 5");

            while (resultSet.next()) {
                constructGame construct = new constructGame();
                construct.setIgrok(resultSet.getString("name"));
                construct.setCount(resultSet.getInt("count"));

                System.out.println(construct);
            }
            resultSet.close();
            statement.close();
            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public String toString() {
        return "Top5 : " +
                "count=" + count +
                ", name='" + igrok + '\'' +
                '}';
    }
}
