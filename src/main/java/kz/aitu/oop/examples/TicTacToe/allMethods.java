package kz.aitu.oop.examples.TicTacToe;

public interface allMethods {

    abstract public String hasWinner(String playerName);

    abstract public  void board(char[][] gameBoard);

    abstract public void pane(char[][] gameBoard, int position, String user);

    abstract public void showFive();
}
