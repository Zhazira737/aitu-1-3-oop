package kz.aitu.oop.examples.TicTacToe;

public class Singletone {
    private static String taken = "It is impossible to take this position! Try again.";

    private static Singletone mysystemInstance = null;

    public static Singletone getError() {
        if(mysystemInstance == null) mysystemInstance = new Singletone();
        return mysystemInstance;
    }

    @Override
    public String toString() {
        return taken;
    }
}
