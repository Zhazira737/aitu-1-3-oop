package kz.aitu.oop.examples.practice5;

import java.io.FileNotFoundException;

public class Main {

    public static void main(String[] args) {
        try {
            int num1 = 0;
            int num2 = Integer.parseInt("Hello World");
            System.out.println(1 / num1);
            System.out.println();
        } catch (ArithmeticException e) {
            System.out.println("something went wrong");
        } catch (NumberFormatException e) {
            System.out.println("number format is not correct");
        } finally {
            System.out.println("show this even there id a mistake");
        }
    }
}
