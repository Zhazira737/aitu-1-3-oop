package kz.aitu.oop.examples.practice5;

public class TaskTwo {

    public static void main(String[] args) {
        try {
            System.out.println(fileExists(90));
            System.out.println(isInt("jjjnj"));
            System.out.println(isDouble(9));
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }


    public static boolean fileExists(Object s) {
        if ( s instanceof String) {
            return true;
        }
        else return false;
    }

    public static boolean isInt(Object i) {
        if ( i instanceof Integer) {
            return true;
        }
        else return false;

    }

    public static boolean isDouble(Object d) {
        if (d instanceof Double) {
            return true;
        }
        else return false;
    }
}
