package kz.aitu.oop.examples.practice22;

import java.sql.SQLException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class RailwayCarriage extends Train {
    String url = "jdbc:mysql://localhost:3306/train"; // or "jdbc:mysql://127.0.0.1:3306/aitu"
    String username = "User1";
    String password = "12345";

    private String passengerName;
    private int id;
    private int cabin;
    private int seat;

    public String getPassengerName() {
        return passengerName;
    }

    public void setPassengerName(String passengerName) {
        this.passengerName = passengerName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCabin() {
        return cabin;
    }

    public void setCabin(int cabin) {
        this.cabin = cabin;
    }

    public int getSeat() {
        return seat;
    }

    public void setSeat(int seat) {
        this.seat = seat;
    }

    public void showPassenger() {
        try {
            Connection connection = DriverManager.getConnection(url, username, password);

            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM carriage");


            List<RailwayCarriage> carriages = new ArrayList<>();
            while (resultSet.next()) {
                RailwayCarriage carriage = new RailwayCarriage();
                carriage.setId(resultSet.getInt("id"));
                carriage.setPassengerName(resultSet.getString("passengerName"));
                carriage.setCabin(resultSet.getInt("cabin"));
                carriage.setSeat(resultSet.getInt("seat"));

                System.out.println(carriage);
            }

            resultSet.close();
            statement.close();
            connection.close();

        } catch (SQLException sql) {
            sql.printStackTrace();
        }
    }
}
