package kz.aitu.oop.examples.practice22;

public class GoodsLocomotive extends Train implements Locomotive {
    private double initialv;
    private double finalv;
    private double time;
    private double length;
    private double acceleration;
    private double distance;

    public double getInitialv() {
        return initialv;
    }

    public void setInitialv(double initialv) {
        this.initialv = initialv;
    }

    public double getFinalv() {
        return finalv;
    }

    public void setFinalv(double finalv) {
        this.finalv = finalv;
    }

    public double getTime() {
        return time;
    }

    public void setTime(double time) {
        this.time = time;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getAcceleration() {
        return acceleration;
    }

    public void setAcceleration(double acceleration) {
        this.acceleration = acceleration;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }


    @Override
    public double avgSpeed(double initialv, double finalv) {
        return 0.5 * (initialv + finalv);
    }

    @Override
    public double timeSpent(double initialv, double finalv, double acceleration) {
        return (finalv -initialv) / acceleration;
    }

    @Override
    public double distance(double time) {
        return avgSpeed(initialv, finalv) * time;
    }

    @Override
    public double acceleration(double initialv, double finalv, double time) {
        return (finalv - initialv) / time;
    }
}
