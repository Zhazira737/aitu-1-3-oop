package kz.aitu.oop.examples.practice22;

import lombok.Data;

@Data
public class Train {
    public double displayGoodsLocomotive() {
        GoodsLocomotive goodsLocomotive = new GoodsLocomotive();
        return goodsLocomotive.avgSpeed(3,4) + goodsLocomotive.distance(5);
    }

    public double displayGatheringLocomotive() {
        GatheringLocomotive gatheringLocomotive = new GatheringLocomotive();
        return gatheringLocomotive.acceleration(0,60, 3);
    }

    public double displayPassengerLocomotive() {
        PassengerLocomotive passengerLocomotive = new PassengerLocomotive();
        return passengerLocomotive.timeSpent(0,180, 5);
    }

    public void displayRailwayCarriage() {
        RailwayCarriage railwayCarriage = new RailwayCarriage();
        railwayCarriage.showPassenger();
    }
}
