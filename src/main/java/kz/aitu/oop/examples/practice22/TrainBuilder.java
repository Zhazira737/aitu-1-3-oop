package kz.aitu.oop.examples.practice22;

public class TrainBuilder {

    private WholeTrain wholeTrain;

    public TrainBuilder(String name) {
        wholeTrain = new WholeTrain(name);
    }

    public TrainBuilder addGatheringLocomotive() {
        wholeTrain.setHasGatheringLocomotive(true);
        return this;
    }

    public TrainBuilder addGoodsLocomotive() {
        wholeTrain.setHasGoodsLocomotive(true);
        return this;
    }

    public TrainBuilder addPassengerLocomotive() {
        wholeTrain.setHasPassengerLocomotive(true);
        return this;
    }

    public WholeTrain build() {
        return wholeTrain;
    }
}
