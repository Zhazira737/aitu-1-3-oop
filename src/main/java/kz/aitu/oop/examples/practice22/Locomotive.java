package kz.aitu.oop.examples.practice22;

public interface Locomotive {

    abstract public double avgSpeed(double initialv, double finalv);

    abstract public double timeSpent(double initialv, double finalv, double acceleration);

    abstract public double distance(double time);

    abstract public double acceleration(double initialv, double finalv, double time);
}
