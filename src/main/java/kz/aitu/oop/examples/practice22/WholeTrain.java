package kz.aitu.oop.examples.practice22;

import lombok.Data;

@Data
public class WholeTrain {

    //pattern
    private String name;
    private boolean hasPassengerLocomotive;
    private boolean hasGoodsLocomotive;
    private boolean hasGatheringLocomotive;

    public WholeTrain(String name) {
        this.name = name;
    }

    public static void main(String[] args) {

        TrainBuilder trainBuilder = new TrainBuilder("AD-11");

        WholeTrain wholeTrain = trainBuilder.addPassengerLocomotive().build();
        System.out.println(wholeTrain);
    }
}
