package kz.aitu.oop.examples.practice22;

import kz.aitu.oop.examples.practice22.GoodsLocomotive;

public class PassengerLocomotive extends GoodsLocomotive {
    @Override
    public double getInitialv() {
        return super.getInitialv();
    }

    @Override
    public void setInitialv(double initialv) {
        super.setInitialv(initialv);
    }

    @Override
    public double getFinalv() {
        return super.getFinalv();
    }

    @Override
    public void setFinalv(double finalv) {
        super.setFinalv(finalv);
    }

    @Override
    public double getTime() {
        return super.getTime();
    }

    @Override
    public void setTime(double time) {
        super.setTime(time);
    }

    @Override
    public double getLength() {
        return super.getLength();
    }

    @Override
    public void setLength(double length) {
        super.setLength(length);
    }

    @Override
    public double getAcceleration() {
        return super.getAcceleration();
    }

    @Override
    public void setAcceleration(double acceleration) {
        super.setAcceleration(acceleration);
    }

    @Override
    public double getDistance() {
        return super.getDistance();
    }

    @Override
    public void setDistance(double distance) {
        super.setDistance(distance);
    }



    @Override
    public double avgSpeed(double initialv, double finalv) {
        return super.avgSpeed(super.getInitialv(), super.getFinalv());
    }

    @Override
    public double timeSpent(double initialv, double finalv, double acceleration) {
        return super.timeSpent(super.getInitialv(), super.getFinalv(), super.getAcceleration());
    }

    @Override
    public double distance(double time) {
        return super.distance(super.getTime());
    }

    @Override
    public double acceleration(double initialv, double finalv, double time) {
        return super.acceleration(super.getInitialv(), super.getFinalv(), super.getTime());
    }
}
