package kz.aitu.oop.examples.assignment3;

public class MyNewSpecialString {
    private String[] array;

    public MyNewSpecialString(String[] array) {
        this.array=array;
    }

    public int length() {
        return array.length;
    }

    private boolean vat(int position) {
        for (int i=0; i<array.length+1; i++) {
            if (i == position){
                return true;
            }
        }
        return false;
    }

    public String valueAt(int position) {
        return (vat(position) ? array[position-1] : "-1");
    }

    public boolean contains(String value) {
        for (int i=0; i<array.length; i++) {
            if (array[i] == value) {
                return true;
            }
        }
        return false;
    }

    public int count(String value) {
        int count = 0;
        for (int i=0; i<array.length; i++) {
            if(array[i] == value) count++;
        }
        return count;
    }

    public String[] show() {
        for (int i=0; i<length(); i++){
            System.out.println(array[i]);
        }
        return array;
    }
}
