package kz.aitu.oop.examples.assignment3;

import java.util.Arrays;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int[] arr = new int[]{1, 2, 3, 123, 1, 1, 1, 2, 1, 3, 1, 2, 1, 2, 1, 2};

        MyString str1 = new MyString(arr);
        System.out.println(str1.length());
        System.out.println(str1.valueAt(4));
        System.out.println(str1.contains(3));
        System.out.println(str1.count(1));
        MyString str2 = new MyString(arr);
        System.out.println(str2.valueAt(19));
        System.out.println(str2.contains(145));
        for (int i=0; i<str1.length(); i++) {
            System.out.println(arr[i]);
        }

        String[] arr1 = new String[]{"banana","apple","orange","oranges","melon","banana"};

        MyNewSpecialString msstr1 = new MyNewSpecialString(arr1);
        System.out.println(msstr1.length());
        System.out.println(msstr1.valueAt(4));
        System.out.println(msstr1.contains("apple"));
        System.out.println(msstr1.count("banana"));
        MyNewSpecialString msstr2 = new MyNewSpecialString(arr1);
        System.out.println(msstr2.valueAt(19));
        System.out.println(msstr2.contains("apples"));
        msstr2.show();
    }
}
