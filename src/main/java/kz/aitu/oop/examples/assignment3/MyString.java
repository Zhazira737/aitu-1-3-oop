package kz.aitu.oop.examples.assignment3;

import java.util.Scanner;

public class MyString {
    private int[] array;

    public MyString(int[] array) {
        this.array=array;
    }

    public int length() {
        return array.length;
    }

    private boolean vat(int position) {
        for (int i=0; i<array.length+1; i++) {
            if (i == position){
                return true;
            }
        }
        return false;
    }

    public int valueAt(int position) {
        return (vat(position) ? array[position-1] : -1);
    }

    public boolean contains(int value) {
        for (int i=0; i<array.length; i++) {
            if (array[i] == value) {
                return true;
            }
        }
        return false;
    }

    public int count(int value) {
        int count = 0;
        for (int i=0; i<array.length; i++) {
            if(array[i] == value) count++;
        }
        return count;
    }
}
