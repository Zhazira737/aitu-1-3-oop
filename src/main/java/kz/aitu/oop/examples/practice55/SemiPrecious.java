package kz.aitu.oop.examples.practice55;

import java.sql.*;

public class SemiPrecious extends Precious implements Stone  {
    String url = "jdbc:mysql://localhost:3306/aitu";
    String username = "User2";
    String password = "zo999lo24to";

    @Override
    public int getId() {
        return super.getId();
    }

    @Override
    public void setId(int id) {
        super.setId(id);
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public void setName(String name) {
        super.setName(name);
    }

    @Override
    public double getCost() {
        return super.getCost();
    }

    @Override
    public void setCost(double cost) {
        super.setCost(cost);
    }

    @Override
    public double getAmount() {
        return super.getAmount();
    }

    @Override
    public void setAmount(double amount) {
        super.setAmount(amount);
    }

    @Override
    public int getNecklace_id() {
        return super.getNecklace_id();
    }

    @Override
    public void setNecklace_id(int necklace_id) {
        super.setNecklace_id(necklace_id);
    }

    @Override
    public double getTotal() {
        return super.getTotal();
    }

    @Override
    public void setTotal(double total) {
        super.setTotal(total);
    }

    @Override
    public void totalPrice() {
        try {
            Connection connection = DriverManager.getConnection(url, username, password);

            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT id, name, cost, amount, necklace_id, (cost * amount)" +
                    " AS total FROM semi_precious");
            while (resultSet.next()) {
                Precious precious = new Precious();
                precious.setId(resultSet.getInt("id"));
                precious.setName(resultSet.getString("name"));
                precious.setCost(resultSet.getDouble("cost"));
                precious.setAmount(resultSet.getInt("amount"));
                precious.setNecklace_id(resultSet.getInt("necklace_id"));
                precious.setTotal(resultSet.getDouble("total"));


                System.out.println(precious);
            }
            resultSet.close();
            statement.close();
            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
