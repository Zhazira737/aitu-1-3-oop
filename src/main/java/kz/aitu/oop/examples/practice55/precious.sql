-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Время создания: Май 19 2020 г., 13:19
-- Версия сервера: 10.4.11-MariaDB
-- Версия PHP: 7.4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `aitu`
--

-- --------------------------------------------------------

--
-- Структура таблицы `precious`
--

CREATE TABLE `precious` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `amount` int(11) NOT NULL,
  `cost` int(11) NOT NULL,
  `necklace_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `precious`
--

INSERT INTO `precious` (`id`, `name`, `amount`, `cost`, `necklace_id`) VALUES
(1, 'Ruby', 2, 100, 1),
(2, 'Sapphire', 1, 300, 2),
(3, 'Aquamarine', 1, 500, 2),
(4, 'Morganite', 3, 250, 3),
(5, 'Zircon Specimens', 4, 50, 3);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `precious`
--
ALTER TABLE `precious`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Test3` (`necklace_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `precious`
--
ALTER TABLE `precious`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `precious`
--
ALTER TABLE `precious`
  ADD CONSTRAINT `Test3` FOREIGN KEY (`necklace_id`) REFERENCES `necklace` (`necklace_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
