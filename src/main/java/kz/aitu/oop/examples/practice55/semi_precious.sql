-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Время создания: Май 19 2020 г., 13:20
-- Версия сервера: 10.4.11-MariaDB
-- Версия PHP: 7.4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `aitu`
--

-- --------------------------------------------------------

--
-- Структура таблицы `semi_precious`
--

CREATE TABLE `semi_precious` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `cost` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `necklace_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `semi_precious`
--

INSERT INTO `semi_precious` (`id`, `name`, `cost`, `amount`, `necklace_id`) VALUES
(1, 'Natural African Amethyst ', 10, 5, 1),
(2, 'Natural Brazilian Amethyst ', 15, 3, 1),
(3, 'Natural Mozambique Garnet', 8, 6, 1),
(4, 'Natural Red Garnet', 20, 10, 2),
(5, 'Natural Peridot', 10, 3, 2),
(6, 'Natural Green Amethyst', 30, 3, 3);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `semi_precious`
--
ALTER TABLE `semi_precious`
  ADD KEY `Test4` (`necklace_id`);

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `semi_precious`
--
ALTER TABLE `semi_precious`
  ADD CONSTRAINT `Test4` FOREIGN KEY (`necklace_id`) REFERENCES `necklace` (`necklace_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
