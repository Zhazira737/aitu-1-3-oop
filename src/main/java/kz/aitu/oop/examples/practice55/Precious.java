package kz.aitu.oop.examples.practice55;

import lombok.Data;
import java.sql.*;

@Data
public class Precious implements Stone{
    String url = "jdbc:mysql://localhost:3306/aitu";
    String username = "User2";
    String password = "zo999lo24to";

    private int id;
    private String name;
    private double cost;
    private double amount;
    private int necklace_id;
    private double total = 0.0;
    private String customer;

    public Precious(String customer) {
        this.customer = customer;
    }

    public Precious() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public int getNecklace_id() {
        return necklace_id;
    }

    public void setNecklace_id(int necklace_id) {
        this.necklace_id = necklace_id;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    @Override
    public void totalPrice() {
        try {
            Connection connection = DriverManager.getConnection(url, username, password);

            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT id, name, cost, amount, necklace_id, (cost * amount)" +
                    " AS total FROM precious");
            double sum = 0;
            while (resultSet.next()) {
                Precious precious = new Precious();
                precious.setId(resultSet.getInt("id"));
                precious.setName(resultSet.getString("name"));
                precious.setCost(resultSet.getDouble("cost"));
                precious.setAmount(resultSet.getInt("amount"));
                precious.setNecklace_id(resultSet.getInt("necklace_id"));
                precious.setTotal(resultSet.getDouble("total"));


                System.out.println(precious);
            }
            resultSet.close();
            statement.close();
            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        return "Precious{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", cost=" + cost +
                ", amount=" + amount +
                ", total=" + total +
                '}';
    }
}
