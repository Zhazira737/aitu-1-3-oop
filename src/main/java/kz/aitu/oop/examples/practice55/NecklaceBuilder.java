package kz.aitu.oop.examples.practice55;

import kz.aitu.oop.examples.practice4.Lisa.AccessoriesForHome;

import java.sql.*;

public class NecklaceBuilder {
    String url = "jdbc:mysql://localhost:3306/aitu";
    String username = "User2";
    String password = "zo999lo24to";

    private Precious precious;

    public NecklaceBuilder(String customer) {
        precious = new Precious(customer);
    }

    public NecklaceBuilder addName(String name) {
        precious.setName(name);
        return this;
    }

    public NecklaceBuilder addAmount(double amount) {
        precious.setAmount(amount);
        return this;
    }

    public NecklaceBuilder addCost(double cost) {
        precious.setAmount(cost);
        return this;
    }

    public NecklaceBuilder addNecklaceId(int necklace_id) {
        precious.setAmount(necklace_id);
        return this;
    }

    public Precious make() {
        return precious;
    }
}
