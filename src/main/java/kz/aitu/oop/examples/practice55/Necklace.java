package kz.aitu.oop.examples.practice55;

import java.sql.*;

public class Necklace extends Precious{
    private int necklace_id;
    private String customer;
    private double total1 = 0.0;
    private double total2 = 0.0;
    private double total = 0.0;

    public int getNecklace_id() {
        return necklace_id;
    }

    public void setNecklace_id(int necklace_id) {
        this.necklace_id = necklace_id;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public double getTotal1() {
        return total1;
    }

    public void setTotal1(double total1) {
        this.total1 = total1;
    }

    public double getTotal2() {
        return total2;
    }

    public void setTotal2(double total2) {
        this.total2 = total2;
    }

    @Override
    public double getTotal() {
        return total;
    }

    @Override
    public void setTotal(double total) {
        this.total = total;
    }

    public void totalCost(int idn) {
        try {
        Connection connection = DriverManager.getConnection(url, username, password);

        Statement statement = connection.createStatement();
        Precious precious = new Precious();
        SemiPrecious semiPrecious = new SemiPrecious();
        ResultSet resultSet = statement.executeQuery("SELECT DISTINCT necklace.necklace_id, customer, " +
                "SUM(precious.cost * precious.amount) as total1 , SUM(semi_precious.cost * semi_precious.amount) as " +
                "total2, (SUM(precious.cost * precious.amount) + SUM(semi_precious.cost * semi_precious.amount)) as total FROM necklace INNER JOIN precious ON necklace.necklace_id = precious.necklace_id " +
                "INNER JOIN semi_precious ON necklace.necklace_id = semi_precious.necklace_id WHERE necklace.necklace_id = " + idn);
        while (resultSet.next()) {
            Necklace necklace = new Necklace();
            necklace.setId(resultSet.getInt("necklace_id"));
            necklace.setCustomer(resultSet.getString("customer"));
            necklace.setTotal1(resultSet.getDouble("total1"));
            necklace.setTotal2(resultSet.getDouble("total2"));
            necklace.setTotal(resultSet.getDouble("total"));

            System.out.println(necklace);
        }
        resultSet.close();
        statement.close();
        connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        return "Necklace{" +
                "necklace_id=" + necklace_id +
                ", customer='" + customer + '\'' +
                ", precious=" + total1 +
                ", semi_precious=" + total2 +
                ", total=" + total +
                '}';
    }


    public static void main(String[] args) {
        Precious precious = new Precious();
        precious.totalPrice();
        System.out.println("--------------------------------------------------");
        SemiPrecious semiPrecious = new SemiPrecious();
        semiPrecious.totalPrice();
        System.out.println("--------------------------------------------------");
        Necklace necklace = new Necklace();
        necklace.totalCost(3);
        System.out.println("--------------------------------------------------");
        NecklaceBuilder necklaceBuilder = new NecklaceBuilder("Lisa");
        Precious precious1 = necklaceBuilder
                .addName("Garnet")
                .addAmount(20)
                .addNecklaceId(1)
                .addCost(500)
                .make();
        System.out.println(precious1);
    }
}
