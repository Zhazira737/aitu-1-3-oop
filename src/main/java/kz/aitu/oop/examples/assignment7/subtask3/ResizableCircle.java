package kz.aitu.oop.examples.assignment7.subtask3;

public class ResizableCircle extends Circle implements Resizable{
    public ResizableCircle(double radius) {
        super(radius);
    }

    public ResizableCircle() {
    }

    @Override
    public String toString() {
        return "ResizableCircle{" + super.toString() +  "}";
    }


    @Override
    public double resize(int percent) {
        return super.getArea() * percent / 100;
    }
}
