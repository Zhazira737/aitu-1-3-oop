package kz.aitu.oop.examples.assignment7.subtask3;

public class TestResizableCircle {
    public static void main(String[] args) {
        ResizableCircle r1 = new ResizableCircle(6);

        System.out.println(r1);
        System.out.println(r1.getArea());
        System.out.println(r1.resize(80));
        System.out.println(r1.getPerimeter());
        System.out.println(r1);

    }
}
