package kz.aitu.oop.examples.assignment7.subtask1;

abstract public class Shape {
    protected String color;
    protected boolean filled;
    private double radius;

    public Shape(String color, boolean filled) {
        this.color = color;
        this.filled = filled;
    }

    public Shape() {
        color = "red";
        filled = true;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public boolean isFilled() {
        return filled;
    }

    public void setFilled(boolean filled) {
        this.filled = filled;
    }

    abstract public double getArea();

    abstract public double getPerimeter();

    @Override
    public  String toString() {
        return "Shape[color=" + this.getColor()
                + ", filled=" + this.isFilled();
    }

}
