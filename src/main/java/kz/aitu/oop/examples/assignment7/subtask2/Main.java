package kz.aitu.oop.examples.assignment7.subtask2;

public class Main {
    public static void main(String[] args) {
        MovablePoint p1 = new MovablePoint(3,4,1,2);
        p1.moveLeft();
        System.out.println(p1);
        MovableCircle kj = new MovableCircle(12, new MovablePoint(2,3,4,2));
        kj.moveUp();
        System.out.println(kj);
    }
}
