package kz.aitu.oop.examples.assignment7.subtask1;

public class Main {
    public static void main(String[] args) {
        Shape s1 = new Circle(5.5, "red", false); // Upcast Circle to Shape
        System.out.println(s1); // which version?
        System.out.println(s1.getArea()); // which version?
        System.out.println(s1.getPerimeter()); // which version?
        System.out.println(s1.getColor());
        System.out.println(s1.isFilled());
        //System.out.println(s1.getRadius()); // the method getRadius is absent in superclass Shape. And since it is
        // upcasting subclass Circle only can do all properties of superclass Shape


        System.out.println("-------------------------------------------------------");

        Circle c1 = (Circle)s1; // Downcast back to Circle
        System.out.println(c1);
        System.out.println(c1.getArea());
        System.out.println(c1.getPerimeter());
        System.out.println(c1.getColor());
        System.out.println(c1.isFilled());
        System.out.println(c1.getRadius());

        System.out.println("-------------------------------------------------------");

        //Shape s2 = new Shape(); //Shape is an abstract class. Used by subclasses, but not by itself
        Shape s3 = new Rectangle(1.0, 2.0, "red", false); // Upcast
        System.out.println(s3);
        System.out.println(s3.getArea());
        System.out.println(s3.getPerimeter());
        System.out.println(s3.getColor());
        //System.out.println(s3.getLength()); // there is no such method (getLength()) in class Shape
        //System.out.println(s2.getColor()); // s2 belongs to Shape's object

        System.out.println("-------------------------------------------------------");

        Rectangle r1 = (Rectangle)s3; // downcast

        System.out.println(r1);
        System.out.println(r1.getArea());
        System.out.println(r1.getColor());
        System.out.println(r1.getLength());

        System.out.println("-------------------------------------------------------");

        Shape s4 = new Square(6.6); // Upcast

        System.out.println(s4);
        System.out.println(s4.getArea());
        System.out.println(s4.getColor());
        //System.out.println(s4.isSide()); // the method getSide() located in Square class, but not in Shape

        System.out.println("-------------------------------------------------------");

        // Take note that we downcast Shape s4 to Rectangle,
        // which is a superclass of Square, instead of Square
        Rectangle r2 = (Rectangle)s4;
        System.out.println(r2);
        System.out.println(r2.getArea());
        System.out.println(r2.getColor());
        //System.out.println(r2.isSide()); // Downcasting means from superclass to subclass. Rectangle is subclass of Shape
        System.out.println(r2.getLength());

        System.out.println("-------------------------------------------------------");

        // Downcast Rectangle r2 to Square
        Square sq1 = (Square)r2;
        System.out.println(sq1);
        System.out.println(sq1.getArea());
        System.out.println(sq1.getColor());
        System.out.println(sq1.isSide());
        System.out.println(sq1.getLength());

    }
}


