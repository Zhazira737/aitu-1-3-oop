package kz.aitu.oop.examples.assignment7.subtask1;

import kz.aitu.oop.examples.assignment7.subtask1.Rectangle;

public class Square extends Rectangle {

    public Square(double side, double width, double length, String color, boolean filled) {
        super(width, length, color, filled);
        this.setWidth(side);
        this.setLength(side);
    }

    public Square(double side) {}

    public Square() {
        super();
    }

    public void setSide(double side) {
        this.length = side;
        //super.length = side;
    }

    public double isSide() {
        return this.getLength();
    }

    @Override
    public void setWidth(double side) {
        super.length = side;
        super.length = side;
    }

    public double getWidth() {
        return width;
    }

    @Override
    public void  setLength(double side) {
        this.length = side;
        this.width = side;
    }

    public double getLength() {
        return length;
    }

    @Override
    public double getArea() {
        return width * length;
    }

    @Override
    public double getPerimeter() {
        return (width + length) * 2;
    }

    public String subOfRec() {
        return super.toString();
    }

    @Override
    public String toString() {
        return "A Square with side=" + isSide()
                + ", which is a subclass of "
                + this.subOfRec();
    }

}
