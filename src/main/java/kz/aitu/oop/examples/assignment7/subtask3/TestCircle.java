package kz.aitu.oop.examples.assignment7.subtask3;

public class TestCircle {
    public static void main(String[] args) {
        Circle c1= new Circle(3);

        System.out.println(c1);
        System.out.println(c1.getArea());
        System.out.println(c1.getPerimeter());

        GeometricObject c2 = new Circle(4);
        System.out.println(c2.getPerimeter()); 
    }
}
