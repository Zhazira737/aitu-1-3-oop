package kz.aitu.oop.examples.assignment7.subtask1;

public class Circle extends Shape {
    protected double radius;

    public Circle(double radius, String color, boolean filled) {
        super(color, filled);
        this.radius = radius;
    }

    public Circle() {
        radius = 1.0;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getRadius() {
        return this.radius;
    }

    @Override
    public double getArea() {
        return (Math.PI * Math.pow(radius, 2));
    }

    @Override
    public double getPerimeter() {
        return (2 * Math.PI * radius);
    }

    public String output() {
        return super.toString();
    }

    @Override
    public String toString() {
        return "Circle[" + output() + ", radius=" + getRadius() + "]";
    }

}
