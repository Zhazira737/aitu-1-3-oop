package kz.aitu.oop.examples.practice7Quiz;

import kz.aitu.oop.examples.practice3.Collector;
import kz.aitu.oop.examples.practice3.EmployeeCollector;
import kz.aitu.oop.examples.practice3.Factory;
import kz.aitu.oop.examples.practice3.ProjectCollector;

public class Main {
    public static void main(String[] args) {
        FoodFactory foodFactory = foodName("cake");
        Food food =foodFactory.getFood();
        System.out.println(food.getType());

    }

    static FoodFactory foodName(String name) {
        if(name.equalsIgnoreCase("cake")) {
            return new CakeFactory();
        } else if(name.equalsIgnoreCase("pizza")) {
            return new PizzaFactory();
        } else {
            throw new RuntimeException(name + " does not exist!");
        }
    }
}
