package kz.aitu.oop.examples.practice7Quiz;

public interface FoodFactory {

    Food getFood();

}
