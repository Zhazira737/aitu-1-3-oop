package kz.aitu.oop.examples.practice7Quiz;

public class Cake implements Food {
    @Override
    public String getType() {
        return "The factory returned class " + getClass().getSimpleName() + "" +
                " Someone ordered a Cake!" ;
    }
}
