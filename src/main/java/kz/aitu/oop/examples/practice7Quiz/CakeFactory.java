package kz.aitu.oop.examples.practice7Quiz;

public class CakeFactory implements FoodFactory {
    @Override
    public Food getFood() {
        return new Cake();
    }
}
