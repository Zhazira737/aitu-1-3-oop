package kz.aitu.oop.examples.practice7Quiz;

import kz.aitu.oop.examples.practice3.Collector;
import kz.aitu.oop.examples.practice3.Factory;
import kz.aitu.oop.examples.practice3.Project;

public class PizzaFactory implements FoodFactory{
    @Override
    public Food getFood() {
        return new Pizza();
    }
}

