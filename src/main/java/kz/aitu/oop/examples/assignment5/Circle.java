package kz.aitu.oop.examples.assignment5;

public class Circle extends Shape{
    double radius;

    public Circle(double radius, String color, boolean filled) {
        this.radius = radius;
    }

    public Circle() {
        radius = 1.0;
    }

    public double isRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getArea() {
        return (Math.PI * Math.pow(radius, 2));
    }

    public double getPerimeter() {
        return (2 * Math.PI * radius);
    }

    public String tt(){
        return super.toString();
    }

    @Override
    public  String toString() {
        return "A Circle with radius " + radius
                + ", which is a subclass of " + this.tt();
    }
}
