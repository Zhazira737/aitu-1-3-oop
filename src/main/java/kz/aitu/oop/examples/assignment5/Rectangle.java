package kz.aitu.oop.examples.assignment5;

public class Rectangle extends Shape {
    double width;
    double length;

    public Rectangle(String color, boolean filled) {
        super(color, filled);
    }

    public Rectangle() {
        width = 1.0;
        length = 1.0;
    }

    public double isWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double isLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getArea() {
        return width * length;
    }

    public double getPerimeter() {
        return 2 * width + 2 * length;
    }

    public String output() {
        return super.toString();
    }

    @Override
    public String toString() {
        return "A Rectangle with width=" + width
                + " and length=" + length
                + ", which is subclass of "
                + this.output();
    }
}
