package kz.aitu.oop.examples.assignment5;

public class Square extends Rectangle {
    public Square(double side,String color, boolean filled) {
        super(color, filled);
        this.setWidth(side);
        this.setLength(side);
        //this.length = side;
        //this.width = side;
    }

    public Square(double side) {
    }

    public double isSide() {
        return this.length;
    }

    public void setSide(double side) {
        this.length = side;
    }

    public Square() {
        super();
    }

    @Override
    public void setWidth(double side) {
        this.width = side;
        this.length = side;
    }

    public double getWidth() {
        return width;
    }

    @Override
    public void  setLength(double side) {
        this.length = side;
        this.width = side;
    }

    public double getLength() {
        return length;
    }

    @Override
    public double getArea() {
        return width * length;
    }

    @Override
    public double getPerimeter() {
        return (width + length) * 2;
    }

    public String subOfRec() {
        return super.toString();
    }

    @Override
    public String toString() {
        return "A Square with side=" + isSide()
                + ", which is a subclass of "
                + this.subOfRec();
    }
}
