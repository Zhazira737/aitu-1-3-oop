package kz.aitu.oop.examples.assignment5;

public class Shape {
    private String color;
    private boolean filled;

    public Shape(String color, boolean filled) {
        this.color = color;
        this.filled = filled;
    }

    public Shape() {
        color = "red";
        filled = true;
    }

    public String isCOLOR() {
        return color;
    }

    public void setCOLOR(String color) {
        this.color = color;
    }

    public boolean isFILLED() {
        return filled;
    }

    public void setFILLED(boolean filled) {
        this.filled = filled;
    }

    @Override
    public  String toString() {
        return "A Shape with color of " + this.isCOLOR()
                + " and it is " + this.isFILLED()
                + " that it is filled";
    }
}
