package kz.aitu.oop;

public class Point {
    private double x;
    private double y;

   /* public static void main(String[] args) {
        Point p1 =new Point();
        p1.setX(4);
        p1.setY(8);

        Point p2 = new Point(6,7);

        System.out.println(p2.distance(p1));
    }*/

    public Point() {
    }

    public double distance(Point tochka) {
        double distance = Math.sqrt(
                Math.pow(tochka.getX() - this.getX(), 2) + Math.pow(tochka.getY() - this.getY(), 2)
        );
        return distance;
        /*this.getX();
        this.getY();
        tochka.getX(); //own x
        tochka.getY(); //own y*/
    }

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }
}
