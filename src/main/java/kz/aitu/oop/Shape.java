package kz.aitu.oop;

import java.util.Scanner;

public class Shape {
    private Point[] arr;
    private int size;
    private int count = 0;

    public Shape(int a) {
        size = a;
        arr = new Point[size];
    }

    public void addPoint(Point points) {
        arr[count++] = points;
    }


    public Point[] getPoints() {
        return arr;
    }

    public double calculatePerimeter() {
        double sum = arr[size - 1].distance(arr[0]);
        for (int i=0; i<size-1; i++) {
            sum+=arr[i].distance(arr[i+1]);
        }
        return sum;
    }

    public double longestSide() {
        double max = -9999;
        for (int i=0; i<size-1; i++) {
            if (arr[i].distance(arr[i+1]) > max){
                max = arr[i].distance(arr[i+1]);
            }
        }
        return  max;
    }

    public double avrgLength() {
        double sum = calculatePerimeter();
        return sum/size;
    }
}