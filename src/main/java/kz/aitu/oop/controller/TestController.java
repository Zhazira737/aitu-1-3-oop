package kz.aitu.oop.controller;

import kz.aitu.oop.AssaignmentOne;
import kz.aitu.oop.Point;
import kz.aitu.oop.Shape;
import kz.aitu.oop.examples.FileReader;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

@RestController
@RequestMapping("/api/test")
public class TestController {


    @GetMapping("/test")
    public ResponseEntity<?> getUserByID() throws Exception {

        Point p1 = new Point(5,8);
        Point p2 = new Point(1,3);
        double distance = p2.distance(p1);

        Scanner sc = new Scanner(System.in);
        Point point = new Point();
        int size = sc.nextInt();
        Shape shape = new Shape(size);
        for (int i=0; i<size; i++) {
            int a = sc.nextInt();
            int b = sc.nextInt();
            point.setX(a);
            point.setY(b);
            shape.addPoint(point);
        }

        Point[] tochki = shape.getPoints();
        double perim = shape.calculatePerimeter();
        double longest = shape.longestSide();
        double avg = shape.avrgLength();

        String result = "";
        result += "Our points: " + tochki +"</br>";
        result += "Perimeter is " + perim + "</br>";
        result += "The longest side is " + longest + "</br>";
        result += "Average length is " + avg + "</br>";
        result += "Distance btw p1 and p2: " + distance;


        return ResponseEntity.ok(result);
    }
}
