package kz.aitu.oop;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner sc = new Scanner (System.in);

        Point point = new Point();

        int size = sc.nextInt();
        Shape shape = new Shape(size);

        for (int i=0; i<size; i++) {
            int a = sc.nextInt();
            int b = sc.nextInt();
            point.setX(a);
            point.setY(b);
            shape.addPoint(point);
        }

        System.out.println(shape.getPoints());
        System.out.println(shape.calculatePerimeter());
        System.out.println(shape.longestSide());
        System.out.println(shape.avrgLength());
    }
}
